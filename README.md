# Gemeinfreies Deutsches Korpus Willkürlicher Texte #

## Zweck des Projekts ##

Ziel des _GeDeuKo_ ist es, der linguistischen und computerlinguistischen Forschung eine breite Ressource heutigen deutschen Sprachgebrauchs zu bieten, auf welcher sie ihre Modelle vergleichen können. Diese Breite wird durch die willkürliche Aufnahme von freien Texten beliebiger Domänen erreicht.

Wir hoffen, dass Nutzer des Korpus ihre eigenen Annotationen der Gemeinschaft hier ebenfalls zur Verfügung stellen.

## Bedingungen zur Nutzung ##

An die Nutzung des Korpus ist lediglich die Bitte geknüpft, in Publikationen auf dieses Repository zu verweisen. Auch wäre es hilfreich, wenn eventuelle Annotationen des ganzen Korpus' oder von Teilen daraus dem GeDeuKo zugänglich gemacht würden. Auf Wunsch kann die Annotation mit Metadaten zum entsprechenden Forscher versehen werden.

## Bedingungen zur Erweiterung ##

Neue Texte sind gerne gesehen und werden jährlich in einer Korpus-Erweiterung veröffentlicht. Beiträge müssen kein Fließtext und auch nicht besonders lang sein. Allerdings sollten die folgenden Eigenschaften beachtet werden.

- Texte sollten auf Deutsch und nicht älter als 20 Jahre sein.
- Sie sollten entweder gemeinfrei bzw. aus dem (legalen) Internet sein oder mit einer expliziten Erlaubnis des Urheberrechtsinhabers eingereicht werden, den Text im Korpus zu verwenden.
- Schließlich sollten Texte nicht extra für das Korpus geschrieben werden!

Jeder Beitrag hilft, aber bestenfalls werden Texte bereits

- in UTF-8 mit LF-Zeilenenden eingereicht und
- nebst einer Nachricht mit Quelle und Metadaten (Autor, Zeit, Textsorte, ...) des Textes und – falls gewünscht – Informationen zum Forscher